import axios from 'axios';

const key = encodeURIComponent('2CC9ULl5OVyeDq7y7qdpEtv6t'),
  secret = encodeURIComponent('NWhxo8BjgTsQITAgmVlH7KxFQpH7dGAUN9OxMRQsbQ0PQFyGU5'),
  bearerToken = window.btoa(`${key}:${secret}`);

const fetchAuthToken = () => {
  axios('https://twittcors.herokuapp.com/', {
    method: 'post',
    headers: {
      'Target-URL': 'https://api.twitter.com/oauth2/token',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': `Basic ${bearerToken}`
    },
    params: {'grant_type':'client_credentials'}
  })
  .then(res => {
    localStorage.setItem('appToken',
    JSON.stringify(res.data.access_token));
  })
}

const fetchNewTweet = (token) => {
  axios('https://twittcors.herokuapp.com/', {
    headers: {
      'Target-URL': 'https://api.twitter.com/1.1/search/tweets.json?q=%2540twitterapi',
      'Authorization': `Bearer ${JSON.parse(localStorage.getItem('appToken'))}`
    }
  }).then(res => console.log(res.data));
}

export {bearerToken, fetchAuthToken, fetchNewTweet};
