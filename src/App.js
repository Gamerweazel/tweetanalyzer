import React, { Component } from 'react';
import { bearerToken, fetchAuthToken, fetchNewTweet } from './twitter';
import './App.css';
import './index.css';

class App extends Component {
  state = {
    fetchedTweet: null
  }

  componentWillMount() {
    if (!localStorage.getItem('appToken')) fetchAuthToken();
    else fetchNewTweet();
  }

  getTweet = () => {

  }

  render() {
    return (
      <div className="App">
        <h3>
          Click to analyze a random tweet.
        </h3>
        <button className='myButton' onClick={() => this.getTweet()}>Random Tweet</button>
      </div>
    );
  }
}

export default App;
